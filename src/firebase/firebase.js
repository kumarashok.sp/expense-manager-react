import * as firebase from "firebase";

const config = {
    apiKey: "AIzaSyASyjO1x2cXfK6XORqa44BqRZkIF4ubj-k",
    authDomain: "daily-expense-ashok.firebaseapp.com",
    databaseURL: "https://daily-expense-ashok.firebaseio.com",
    projectId: "daily-expense-ashok",
    storageBucket: "daily-expense-ashok.appspot.com",
    messagingSenderId: "807916611365"
};

if (!firebase.apps.length) {
    firebase.initializeApp(config);
}

const db = firebase.database();
const auth = firebase.auth();

export { auth, db };
